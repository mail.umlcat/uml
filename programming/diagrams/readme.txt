readme.txt
==========

Template Resources for working with non U.M.L. Modeling diagrams,
such as Entity-Relationship, Coad-Yourdon, and others,
using Libre Office / Open Office Draw application,
(Vector Drawing Editor).

In order to create a non template, new file using an existing template,
just open the required template file,
it will be loaded as am unamed new non template file,
and will request a new filename when saved.

If you want to edit the existing template,
and keep it as a template, from a blank file,
go to the "File/Templates/Edit Templates..." english menu option,
or the equivalent in another language.




